<?php

$plugin = array(
  'single' => TRUE,
  'title' => t('Hellosign Embedded Form'),
  'description' => t('Shows a signing form with basic configuration options.'),
  'category' => t('Widgets'),
  'edit form' => 'hellosign_pane_embed_edit_form',
  'render callback' => 'hellosign_pane_embed_render',
  'all contexts' => TRUE,
  'admin info' => 'hellosign_pane_embed_admin_info',
  'defaults' => array(
		'embed_url' => '',
		'signature_id' => '',
		'doctitle' => '',
		'docsubject' => '',		
		'signfile' => '',
		'signusers' => '',		
    'redirectUrl' => '',
    'allowCancel' => '',
    'container' => '',
    'height' => '',
  )
);

/**
 * 'admin info' callback for panel pane.
 */
function hellosign_pane_embed_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
    $block->content = t('Embeded Hellosign Form - redirects to <em>@redirectUrl</em> , allowed to cancel = <em>@allowCancel</em>, container = <em>@container</em>, height = <em>@height</em>.', array(
      '@redirectUrl' => $conf['redirectUrl'],
      '@allowCancel' => $conf['allowCancel'],
      '@container' => $conf['container'],
      '@height' => $conf['height'],
    ));
    return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 */
function hellosign_pane_embed_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

/*
  $form['client_id'] = array(
    '#title' => t('Hellosign Client ID'),
    '#description' => t('Your hellosign client ID'),
    '#type' => 'textfield',
    '#default_value' => $conf['client_id'],
    '#required' => TRUE,
  );
*/

  $form['embed_url'] = array(
    '#title' => t('Hellosign Embed URL'),
    '#description' => t('The embed URL provided by hellosign'),
    '#type' => 'textfield',
    '#default_value' => $conf['embed_url'],
  );	
  $form['signature_id'] = array(
    '#title' => t('Hellosign signature ID'),
    '#description' => t('Document signature ID'),
    '#type' => 'textfield',
    '#default_value' => $conf['signature_id'],
  );
  $form['doctitle'] = array(
    '#title' => t('Doc title'),
    '#description' => t('Document title'),
    '#type' => 'textfield',
    '#default_value' => $conf['doctitle'],
  );
	
  $form['docsubject'] = array(
    '#title' => t('Doc subject'),
    '#description' => t('Document description'),
    '#type' => 'textfield',
    '#default_value' => $conf['docsubject'],
  );
	
  $form['signfile'] = array(
    '#title' => t('Sign file'),
    '#description' => t('File URI'),
    '#type' => 'textfield',
    '#default_value' => $conf['signfile'],
  );
	
  $form['signusers'] = array(
    '#title' => t('Users to sign file'),
    '#description' => t('Field with users reference'),
    '#type' => 'textfield',
    '#default_value' => $conf['signusers'],
  );	

  $form['redirectUrl'] = array(
    '#title' => t('Redirect URL'),
    '#description' => t('Where to send the user after signing'),
    '#type' => 'textfield',
    '#default_value' => $conf['redirectUrl'],
    '#required' => FALSE,
  );

  $form['allowCancel'] = array(
    '#title' => t('Allow cancel of signing'),
    '#description' => t('Provide a cancel button'),
    '#type' => 'checkbox',
    '#default_value' => $conf['allowCancel'],
    '#required' => FALSE,
  );

  $form['container'] = array(
    '#title' => t('Container'),
    '#description' => t('DOM element used to contain the signing form'),
    '#type' => 'textfield',
    '#default_value' => $conf['container'],
    '#required' => FALSE,
  );

  $form['height'] = array(
    '#title' => t('Height'),
    '#description' => t('The height of the iframe (depends on container being set)'),
    '#type' => 'textfield',
    '#default_value' => $conf['height'],
    '#required' => FALSE,
  );

  return $form;
}

/**
 * The validation for edit form.
 */
function hellosign_pane_embed_edit_form_validate($form, &$form_state) {
	//dpm($form_state['values'], validation);
	if(empty($form_state['values']['embed_url']) && empty($form_state['values']['signfile']) && empty($form_state['values']['signature_id'])){
		form_set_error($form_state['values']['embed_url']);
		form_set_error($form_state['values']['signature_id']);
		form_set_error($form_state['values']['signfile']);
		drupal_set_message(t('One of "embed url", "signature ID" or "sign file URI" fields should be populated'), 'error');
	}
}

/**
 * The submit form stores the data in $conf.
 */
function hellosign_pane_embed_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type)
 * See ctools_plugin_examples for more advanced info
 */
function hellosign_pane_embed_render($subtype, $conf, $panel_args, $contexts = NULL) {
  $block = new stdClass();
	
  // initial content is blank
  $block->title = '';
  $block->content = '';

  drupal_add_js('//s3.amazonaws.com/cdn.hellofax.com/js/embedded.js', 'external');

  if ($contexts) {
    foreach ($conf as $key => $value) {
     $conf_replaced[$key] = ctools_context_keyword_substitute($value, array(), $contexts);
    }
  }
	/*
	$conf_replaced['signfile'] = ctools_context_keyword_substitute($conf['signfile'], array(), $contexts, array('sanitize' => FALSE));
	$conf_replaced['signusers'] = ctools_context_keyword_substitute($conf['signusers'], array(), $contexts, array('sanitize' => FALSE));
	*/
	
	// Get file URI
	$fpath = $conf_replaced['signfile'];
	global $base_url;
	$file_rel_path = str_replace($base_url . '/', '', $fpath);
	
	$client_id = variable_get('hellosign_client_id', '');
  $allowCancel = ($conf_replaced['allowCancel'] == '1') ? 'allowCancel: true, ' : 'allowCancel: false, ';
  $redirectUrl = ($conf_replaced['redirectUrl'] != '') ? 'redirectUrl: "'.$conf_replaced['redirectUrl'].'", ' : null;
  $container = ($conf_replaced['container'] != '') ? 'container: '.$conf_replaced['container'].', \n': null;
  $height = ($conf_replaced['height'] != '') ? 'height: '.$conf_replaced['height'].', \n' : null;
	if(!empty($conf_replaced['embed_url'])){
		$url = $conf_replaced['embed_url'];
	}
	elseif(!empty($conf_replaced['signature_id'])){
		$req = hellosign_get_embed_url($conf_replaced['signature_id']);
		$url = $req['url'];
	}
	else{
		$url = _hellosign_pane_get_embed_url($file_rel_path, $conf_replaced['signusers'], $conf_replaced['doctitle'], $conf_replaced['docsubject']);
	}


  // Add in the content
  $block->content .= '
     <script type="text/javascript">
			HelloSign.init("'.$client_id.'");
			HelloSign.open({
					url: "'.$url.'", '. $allowCancel . $redirectUrl . $container . $height .'
					messageListener: function(eventData) {
							//  do something
					}
			});
    </script>';


  return $block;
}
